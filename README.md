
# 2021 Roguelike Tutorial Jam

[Reddit](https://www.reddit.com/r/roguelikedev/comments/o5x585/roguelikedev_does_the_complete_roguelike_tutorial/) - [Twitter](https://twitter.com/GridSageGames/status/1407493165100113922?s=20)

This (basic) roguelike is written in [Nim](https://nim-lang.org/) using [BearLibTerminal](https://github.com/cfyzium/bearlibterminal) 
## Run the game

You'll need to Nim. You'll also need BearLibTerminal 15.7 installed (this differs depending on your operating system). 

Then, simply type `nimble run` to execute it.

## Roadmap

### Tutorial

- [x] Part 0 - Setting up
- [x] Part 1 - Drawing the ‘@’ symbol and moving it around
- [/] Part 2 - The generic Entity, the render functions, and the map
- [ ] Part 3 - Generating a dungeon
- [ ] Part 4 - Field of view
- [ ] Part 5 - Placing enemies and kicking them (harmlessly)
- [ ] Part 6 - Doing (and taking) some damage
- [ ] Part 7 - Creating the Interface
- [ ] Part 8 - Items and Inventory
- [ ] Part 9 - Ranged Scrolls and Targeting
- [ ] Part 10 - Saving and loading
- [ ] Part 11 - Delving into the Dungeon
- [ ] Part 12 - Increasing Difficulty
- [ ] Part 13 - Gearing up


