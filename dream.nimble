# Package

version       = "0.0.1"
author        = "Mathew Fournier"
description   = "A simple roguelike, built to learn Nim."
license       = "MIT"

srcDir        = "src"
bin           = @["dream"]

# Dependencies

requires "nim >= 1.4.8"
requires "https://github.com/zacharycarter/blt-nim.git"

task run, "builds the app and runs it":
  exec "nim c -o=build/dream -r src/dream.nim"
