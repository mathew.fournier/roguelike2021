import
  blt,
  terminal,
  ui,
  game,
  input

proc run(game: Game): void =
  let terminal = bearLibTerminal()
  terminal.draw(game.map)
  terminal.draw(game)
  var key = terminalRead()
  while key != TK_CLOSE and key != TK_ESCAPE:
    terminal.handleInput(game, key)
    key = terminalRead()

if isMainModule:
  discard terminalOpen()
  run(startGame())
  terminal_close()
