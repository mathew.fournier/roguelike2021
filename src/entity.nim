type
  Vector* = tuple[x: int, y: int]
  Position* = tuple[x: int, y: int]

  Entity* = ref object
    position*: Position
    glyph*: string

  Player* = Entity

proc translate*(e: Entity, v: Vector) =
  e.position = ((e.position.x + v.x), (e.position.y + v.y))
