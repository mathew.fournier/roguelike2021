import
  blt,
  map,
  map/generate,
  entity,
  random
from times import getTime, toUnix, nanosecond 

type
  Game* = object
    player*: Player
    map*: Map
    rand*: Rand

proc startGame*: Game =
  let now = getTime()
  return Game(
    map: generateMap(terminal_state(TK_WIDTH),
                     terminal_state(TK_HEIGHT)),
    player: Player(position: (2, 2), glyph: "@"),
    rand: initRand(now.toUnix * 1_000_000_000 + now.nanosecond))
