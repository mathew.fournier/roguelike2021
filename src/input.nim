import
  blt,
  game,
  terminal,
  ui,
  strformat
  

proc handleInput*(t: Terminal, game: Game, key: int): void =
  echo fmt"key pressed: {key}"
  case key:
    of TK_UP: t.move(game.player, (x: 0, y: -1))
    of TK_DOWN: t.move(game.player, (x: 0, y: 1))
    of TK_LEFT: t.move(game.player, (x: -1, y: 0))
    of TK_RIGHT: t.move(game.player, (x: 1, y: 0))
    else: discard
  t.draw(game)
