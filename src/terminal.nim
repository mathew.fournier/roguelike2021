import 
  blt

# If you forget to implement one of these the default is nil so it 
# becomes a runtime error :(
type
  Terminal* {.requiresInit.} = object
    layer*: proc(i: int): void
    print*: proc(x: int, y: int, s: string)
    refresh*: proc(): void
    clearTile*: proc(x: int, y: int)
    clearArea*: proc(x: int, y: int, w: int, h: int)
  

proc bbLayer(i: int): void = 
  terminal_layer(i.cint)

proc bbDraw(x: int, y: int, s: string) = 
  discard terminal_print(x.cint, y.cint, s)

proc bbRefresh(): void = 
  terminal_refresh()

proc bbClearArea(x: int, y: int, w: int, h: int): void =
  terminal_clear_area(x.cint, y.cint, w.cint, h.cint)

proc bbClearTile(x: int, y: int): void =
  bbClearArea(x, y, 1, 1)

proc bearLibTerminal*(): Terminal =
  Terminal(
    layer: bbLayer,
    print: bbDraw,
    refresh: bbRefresh,
    clearTile: bbClearTile,
    clearArea: bbClearArea
  )
