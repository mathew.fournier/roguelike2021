type
  TileType* {.pure.} = enum
    Floor,
    Wall

  Tile* = object
    tileType*: TileType

proc glyph*(tile: Tile): string =
  case tile.tileType:
    of TileType.Floor:
      "."
    of TileType.Wall:
      "X"
