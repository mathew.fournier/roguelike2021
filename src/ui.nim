import
  terminal,
  map,
  tile,
  game,
  entity

proc draw*(t: Terminal, map: Map): void =
  t.layer(1)
  for i in 0 ..< map.tiles.len:
    var x, y: int
    x = i %% map.width
    y = i /% map.width
    t.print(x, y, map.tiles[i].glyph())

proc draw*(t: Terminal, entity: Entity): void =
  t.layer(2)
  t.print(
    entity.position.x.cint,
    entity.position.y.cint,
    entity.glyph
  )

proc remove*(t: Terminal, entity: Entity): void =
  t.layer(2)
  t.clearTile(entity.position.x, entity.position.y)

proc move*(t: Terminal, player: Player, vec: Vector): void =
  t.remove(player)
  player.translate(vec)
  t.draw(player)

proc draw*(t: Terminal, game: Game): void =
  # t.draw(game.map)
  t.draw(game.player)
  t.refresh()
