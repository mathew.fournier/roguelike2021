# This is just an example to get you started. You may wish to put all of your
# tests into a single file, or separate them into multiple `test1`, `test2`
# etc. files (better names are recommended, just make sure the name starts with
# the letter 't').
#
# To run these tests, simply execute `nimble test`.

import unittest

import entity
suite "Entity operations":
  test "translate":    
    let e = Entity(position: (2, 2))
    e.translate((1, -1))
    check e.position == (3, 1)
